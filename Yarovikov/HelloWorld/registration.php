<?php
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
  ComponentRegistrar::MODULE, 
  'Yarovikov_HelloWorld', 
  __DIR__
);